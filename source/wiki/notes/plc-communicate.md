---
layout: wiki
wiki: notes
title: PLC通信相关
tags: []
categories: []
date: 2023-12-04 18:13:10
description:
---

## PLC接口和默认通讯参数相关
默认ip地址，端口等
https://docs.emqx.com/zh/neuronex/latest/configuration/south-devices/ethernet-ip/ethernet-ip.html

## 各种plc hmi地址规则
https://zhuanlan.zhihu.com/p/3739517

## java s7库
https://gitee.com/xingshuang/iot-communication/blob/master/tutorial/README-S7-CN.md#14-%E4%B8%8A%E4%BC%A0%E4%B8%8B%E8%BD%BD%E6%8C%87%E4%BB%A4

## LS XGT 编程口/串口协议
串口支持编程，还有一个usb口
https://lsis.com.cn/Cn/Download/download/catid/89.html


## 三菱编程口FX协议报文
https://blog.csdn.net/john_liu_/article/details/102659077