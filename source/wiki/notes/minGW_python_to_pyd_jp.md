---
title: MinGWを使ってPythonをpydファイルにコンパイルする際の経験整理
tags: [Python, C·C++]
categories: [開発記録, Python]
date: 2023-12-12 13:48:20
description:
layout: wiki
wiki: notes
---

VSツールをインストールする必要はなく、pydファイルの使用説明。

## scoopを使ってMinGWを自動的にインストール、設定する

パッケージマネージャーのscoopでMinGWをインストールします。このステップは、手動でmingwをダウンロードして設定することも可能です。

``` 
scoop install mingw
```

## CythonとSetuptoolsのライブラリをインストールする

```
pip install setuptools -i 
pip install cython -i 
```

## setup.pyでコンパイル内容を設定する

```python
from setuptools import setup
from Cython.Build import cythonize
from setuptools.extension import Extension

# 複数の拡張モジュールを定義'
extensions = [
    Extension(name="main_screen_ini_gbk", sources=["main_screen_ini_gbk.py"]),
    Extension(name="tcpserver_ini_gbk", sources=["tcpserver_ini_gbk.py"]),
    # Extension(name="mylib1", sources=["mylib1.py"]),
    # さらに多くのモジュールを追加できます
]

setup(
    ext_modules=cythonize(extensions, language_level=3)
)
# python setup.py build_ext --inplace  --compiler=mingw32
```

## 以下のコマンドを実行してpydファイルにコンパイルする

64ビットのMinGWをインストールしていても、--compiler=mingw32で実行する。

```
python setup.py build_ext --inplace  --compiler=mingw32
```

## pydファイルをインポートして使用する

コンパイルが完了したpydファイルの名前は「スクリプト名.cp38-win_amd64.pyd」のような形式になります。生成されたpydファイル名は変更せず、インポートする際はスクリプト名の部分のみを使用します。直接.pyスクリプトをインポートする場合と同じです。

**注意：.pydは外部ファイルとしてインポートされるだけで、直接実行することはできません。別の.pyスクリプトを書いてpydファイルをインポートし、エントリーポイントとして使用する必要があります。.pydとpyが同時に存在する場合、実行時には.pydファイルが優先的に実行されます。**

![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2023/12/12_14_13_4_20231212141304.png)

```python
# pydパッケージをインポート
import main_screen_ini_gbk 
# pydに封じ込められたメソッドを使用
main_screen_ini_gbk.push2NextHome(tsclibrary) 
```
