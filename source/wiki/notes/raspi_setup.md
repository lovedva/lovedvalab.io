---
layout: wiki  # 使用wiki布局模板
wiki: notes # 这是项目名
title: 树莓派常用配置
# menu_id: note
sidebar: ['toc', 'related','recent']
---

## 常用命令
### 连接wifi  
sudo iwlist wlan0 scan | grep ESSID
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
network={
    ssid="your_wifi_network"
    psk="your_wifi_password"
}
- 重启wifi(还是得重启)
sudo wpa_cli -i wlan0 reconfigure

- 检查wifi状态
iwconfig wlan0
ip addr show wlan0


## 常用配置
- aptget换清华源 (暂时有问题)
https://www.cnblogs.com/cheney-970918/p/13781381.html

