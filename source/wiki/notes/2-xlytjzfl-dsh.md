---
layout: post
title: 《心灵源头的教子方略》读书会合集
tags: [学习笔记,读书会]
categories: [资料收藏,备忘,学习笔记,读书会]
date: 2024-06-20 20:49:56
description:
cover: https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/06/20_21_41_21_202406202141977.png
---

大庆家庭教育读书会录制  
书名：心灵源头的教子方略  作者：沈健  
<img src="https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/06/20_21_41_21_202406202141977.png" height=35%>  
[思维导图笔记-心灵源头的教子方略.pdf 点击下载](https://gitlab.com/lovedva/lovedva.gitlab.io/-/raw/main/source/assets/attachments/%E6%80%9D%E7%BB%B4%E5%AF%BC%E5%9B%BE-%E5%BF%83%E7%81%B5%E6%BA%90%E5%A4%B4%E7%9A%84%E6%95%99%E5%AD%90%E6%96%B9%E7%95%A5.pdf?ref_type=heads)

## 收听平台
- 喜马拉雅(更新中)
https://www.ximalaya.com/album/82048486
- Bilibili(免登录)
https://www.bilibili.com/video/BV1NbaheTE1J/
- Bilibili音频(免登录)
https://www.bilibili.com/audio/am33727245
- 蜻蜓FM
http://share.qingting.fm/vchannels/491027
- 网页收听&下载音频

如果链接失效，请在下方评论。

