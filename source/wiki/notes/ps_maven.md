---
layout: wiki
wiki: notes
title: Maven 常用命令
tags: [Maven,Java]
categories: [开发记录,Java]
date: 2023-11-29 13:46:26
# description:  
---

## 创建空白项目
- 配置阿里云镜像
  https://www.cnblogs.com/zzvar/articles/14596750.html
- 配置repo路径
```
mvn archetype:generate -DgroupId=com.jna.app -DartifactId=jna-app -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false

```



## 下载依赖
`
mvn dependency:resolve
`
## 清理更新依赖,生成jar包
`
mvn clean install
`
## 查看项目依赖
`
mvn dependency:tree
`