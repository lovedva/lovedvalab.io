---
layout: wiki  # 使用wiki布局模板
wiki: notes # 这是项目名
title: termux常用配置
# menu_id: note
sidebar: ['toc', 'related','recent']
---

## 常用命令
- 设置国内源
```
termux-change-repo
```
- 取得内部储存访问权限
termux-setup-storage
- 启动时自动进入特定目录
nano ~/.bashrc
## git 相关
- 快速比较远程本地
git fetch
git status


## 配置vim
- 显示行号
:set number
