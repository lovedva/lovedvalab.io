---
title: 自动安装环境shell脚本使用和运维基础使用讲解
tags: [shell,linux,运维]
categories: [开发记录,系统运维]
date: 2024-3-27 14:10:15
description:
---

## 准备和说明
0. 视频讲解和补充
   https://m.acfun.cn/v/?ac=44429848&sid=5c5efb14943a4203
1. 确认有网。  
   依赖程序集，官网只提供32位压缩包，手动编译安装后，在64位机上识别不出来，只能用yum包管理从源上拉下来，**需要有网**。  
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/04/2_13_21_42_202404021321049.png)
  
2. 安装unzip,git,wget，用来解压安装包，如果服务器已有会自动跳过安装
    ```
    yum -y install unzip
    yum -y install git 
    yum -y install wget
    ```
3. **使用root登录并执行安装脚本，注意安全措施。** （只有root权限才能在脚本内刷新当前的环境变量）
## 生成安装用配置文件并手动配置
- 执行0-2脚本在当前目录生成 install_config.txt，第一个参数为redis密码，第二个参数为mysql密码  
 **这里生成的密码安装之后只能进程序手动修改，不能通过改此配置文件修改密码！！！**
 **这里生成的密码安装之后只能进程序手动修改，不能通过改此配置文件修改密码！！！**
 **这里生成的密码安装之后只能进程序手动修改，不能通过改此配置文件修改密码！！！**
```
sh 0-1_generate_config_file.sh  666 7756
```

- 用文本编辑器打开生成的 install_config.txt 配置文件，进行ip，端口，密码等相关配置。
- 默认生成的配置为当前机器的内网IP，如果多个程序装在不同机器上，需要手动配置相关IP
### redis配置说明
```
 "redis": {
        "redis_dir": "/data/work/redis",
        "redis_tar": "redis-6.0.6.tar.gz",
        "redis_bind_ip": "0.0.0.0",    //监听IP地址
        "redis_password" : "123456"   
    },
```
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/04/2_11_6_59_202404021106289.png)

### mysql配置说明
配置root密码，以及配置文件路径
```
    "mysql": {
      "mysql_dir": "/data/work/mysql",
      "mysql_tar_xz": "mysql-8.0.20-linux-glibc2.12-x86_64.tar.xz",
      "mysql_new_password": "123456",   //配置密码
      "mysql_config_file": "/etc/my.cnf"
    },
```


### nacos 配置说明
-  mysql在本地为内网IP，在其他位置写入ip地址和端口号   
-  redis 同上
```
"nacos": {
      "nacos_dir": "/data/work/nacos",
      "nacos_tar_gz": "nacos-server-1.2.1.tar.gz",
      "mysql_driver": "mysql-connector-java-8.0.20.jar",
      "nacos_mysql_ip": "127.0.0.1:3306",  #mysql在本地为127.0.0.1，在其他位置写入ip地址和端口号   
      "redis_config": {
        "spring.redis.host": "127.0.0.1",  #mysql在本地为127.0.0.1，在其他位置写入ip地址和端口号   
        "spring.redis.port": 6379   #redis端口号
      }
    }
```
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/04/2_11_23_34_202404021123919.png)


- 安装完成后需要访问网页版UI手动导入配置文件**
redis相关配置已经写入 ~/conf/application.properties 文件中，若不生效就手动进WebUI进行配置, ip:8848/nacos   
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/04/2_13_44_21_202404021344161.png)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/04/2_13_38_54_202404021338403.png)

### seata配置说明
- **docker 安装1.3.0启动报错，连不上mysql报jdbc错误，用nacos模式启动也失败，需要nacos 2.x 版本以上**  
- mysql，redis，nacos 服务若装在非本机上，配置相应ip和端口号。
- seata_host_ip 配置向注册中心时使用的IP，默认使用机器内网IP 
```
    "seata":{
        "seata_dir": "/data/work/seata",
        "seata_tar": "seata-server-1.3.0.tar.gz",
        "seata_host_ip": "127.0.0.1",  //向注册中心时使用的IP
        "seata_mysql_ip": "127.0.0.1:3306",
        "seata_redis_host": "127.0.0.1",
        "seata_redis_port": "6379",
        "seata_nacos_ip": "127.0.0.1:8848"

    }
```

### docker 配置说明
- docker_fastdfs_ip : fastdfs ip地址配置
```
"docker":{
        "docker_work_dir": "/data/work/dockerapps",
        "docker_fastdfs_ip": "127.0.0.1"
    }
```

## 安装方法&测试安装结果
在当前会话中更新环境变量使环境变量生效，或者重启。
```
source /etc/profile
```
### 用screen会话无人值守安装
- 安装Screen
```
yum install -y screen
```
- 创建和使用screen 会话/窗口
```
screen -S 1_install
screen -r <会话名> #恢复会话
screen -ls #列出当前所有会话
```
分离和恢复会话：当需要暂时离开会话时，可以使用快捷键Ctrl+a d（即按住Ctrl键，然后依次按a和d键）来分离当前会话，此时会话中的程序仍将继续运行。当需要恢复会话时，可以使用screen -r <会话名>命令，其中<会话名>是之前创建的会话的名称。如果只有一个会话，可以简化为screen -r。  

- 退出会话
```
screen -X -S 1_install quit
# screen -X -S <session name> quit
```

### 自动下载并安装到本机
**修改数据库密码**，复制到控制台粘贴，回车即可，**注意不要中断安装过程**。  
`cd ~` 进入用户主目录
```
sudo yum -y install unzip #安装unzip
sudo yum -y install wget #安装wget
sudo yum -y install git  # 安装git
# 下载安装包
git clone https://gitee.com/dvaloveu/install_env.git
cd install_env
sudo wget http://v.dihuait.com/install_env/jdk-8u231-linux-x64.tar.gz
sudo wget http://v.dihuait.com/install_env/mysql-8.0.20-linux-glibc2.12-x86_64.tar.xz
# 生成安装用配置文件json
sh 0-1_generate_config_file.sh qwe.1 asd!  #这里修改密码
sudo sh 0-2_excute_all.sh 
#执行所有脚本安装到本机↑

```

### 手动安装所有程序在本机
1. 执行 0-1_generate_config_file.sh 【redis密码】 【mysql密码】 生成安装用配置文件install_config.json 
```
0-1_generate_config_file.sh pass_redis pass_mysql #记得修改密码换成自己的
```

2. 将 install_config.txt ，安装脚本，安装包，放在相同目录下，然后执行各个安装脚本，文件结构如下
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/04/18_17_4_22_202404181704018.png)
执行 0-2_excute_all.sh 将一次执行所有脚本安装在本机
```
sudo sh 0-2_excute_all.sh 
```
### 多主机安装&同时执行多个命令
多个命令用分号隔开，然后一次执行，举例如下
```
sudo sh 1_install_jdk.sh; sudo sh 2_install_maven.sh; sudo sh 3_install_docker-compose.sh;sudo sh 4_install_nginx.sh; sudo sh 5_install_redis.sh
```
```
sudo sh 7_install_nacos.sh; sudo sh 8_install_seata.sh; sudo sh 9_install_dockers.sh
```
多主机安装需要把相应安装包和安装脚本和 install_config.txt 放在同一目录下，如果缺少相关文件会提示。

### 开机延迟逐个重启docker所有容器
待更新
### 1. jdk
需要手动更新环境变量在当前会话中生效，或重启
```
source /etc/profile  
java -version
javac -version
```
### 2. Maven
```
source /etc/profile
mvn -v
```

### 3. docker-compose 
```
docker-compose -version
```
### 4. nginx
```
echo "sudo ps aux|grep nginx"
echo "sudo netstat -tulpn | grep :80"

```
### 5. redis
在4G以下内存安装过程如果报错，为内存不足导致，请务必重启机器以释放内存，然后重新执行本脚本。


- 查看剩余内存
```
free -m
# 重启 
sudo reboot
```
- 获取redis进程
```
netstat -tuln | grep 6379
sudo ps aux | grep redis-server
```

### 7. Nacos
```
echo "sudo ps aux|grep nacos"
echo "sudo netstat -tulpn | grep :8848"
echo "web访问 ip:8848/nacos"
```
### 8. seata
```
echo "sudo ps aux|grep seata"
echo "sudo netstat -tulpn | grep :8091"
echo "访问nacos web页面查看seata服务"
```

## 运维命令
### systemctl系统服务相关
- service 文件目录  
  `/etc/systemd/system`   
  `cd /etc/systemd/system`  
  `ls /etc/systemd/system`  
- 查看服务启动状态
```
systemctl status redis
```
- 开启，终止，重启各个程序服务
```
systemctl start redis
systemctl stop redis
systemctl restart redis
```
- 刷新服务，启动服务
```
systemctl daemon-reload 
systemctl enable mysql
systemctl daemon-reload 
systemctl enable nacos
systemctl daemon-reload 
systemctl enable nginx
systemctl daemon-reload 
systemctl enable redis
```
- 查看服务单元是否存在
```
systemctl list-unit-files |grep mysql
systemctl list-unit-files |grep redis
systemctl list-unit-files |grep nacos
systemctl list-unit-files |grep nginx
systemctl list-unit-files |grep seata
```



### docker相关

#### docker配置参考
https://www.cnblogs.com/chuyiwang/p/17577020.html
#### 卸载清理docker
```
sudo systemctl stop docker
sudo yum remove docker-ce docker-ce-cli containerd.io
sudo yum remove docker-common

sudo rm -f /usr/bin/docker /usr/bin/dockerd
sudo rm -rf /var/lib/docker
sudo rm -rf /etc/docker

sudo rm -rf /var/run/docker
sudo rm -rf /var/log/docker
```

## 记录
docker composer 没有找到 1.22版本，github只有1.20版，运维提供本地文件
https://github.com/docker/compose/releases?page=21

https://blog.csdn.net/chaogaoxiaojifantong/article/details/128028769
