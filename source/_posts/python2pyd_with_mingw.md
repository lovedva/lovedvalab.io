---
title: MinGW编译Python至pyd踩坑整理
tags: [Python,C·C++]
categories: [开发记录,Python]
date: 2023-12-12 13:48:20
description:
sidebar: ['toc', 'related','recent']
---
不需要安装VS工具，pyd使用说明。
## 用scoop自动安装配置MinGw
需要魔法，用包管理scoop安装不需要手动配置。这一步可以自行下载mingw64然后手动配置。
``` 
scoop install mingw
```
手动安装mingw32也可以，官方地址下载安装：https://sourceforge.net/projects/mingw/    
安装方法：https://blog.csdn.net/HandsomeHong/article/details/120803368  
↑记得最后要添加一下环境变量  
如果官网下载太慢可以用我提供的备份：https://gitee.com/dvaloveu/lovedva/blob/master/blog_attachments/mingw-get-setup.exe  
下载链接失效请私信或者文章最下方评论，QQ465318701 答案:没有 han_dagou@outlook.com  

## 安装Cython，Setuptools第三方库
关闭魔法，使用清华源
```
pip install setuptools -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install cython -i https://pypi.tuna.tsinghua.edu.cn/simple
```
## 编写setup.py配置编译内容
```python
from setuptools import setup
from Cython.Build import cythonize
from setuptools.extension import Extension

# 定义多个扩展模块
extensions = [
    Extension(name="main_screen_ini_gbk", sources=["main_screen_ini_gbk.py"]),
    Extension(name="tcpserver_ini_gbk", sources=["tcpserver_ini_gbk.py"]),
    # Extension(name="mylib1", sources=["mylib1.py"]),
    # 可以继续添加更多的模块
]

setup(
    ext_modules=cythonize(extensions, language_level=3)
)
# python setup.py build_ext --inplace  --compiler=mingw32
```

## 执行以下命令编译成pyd文件
即使安装的64位minGW也用 --compiler=mingw32
```
python setup.py build_ext --inplace  --compiler=mingw32
```

## 使用引入pyd文件
编译完成的pyd文件名称为 “脚本名.cp38-win_amd64.pyd” 类似的格式,不要修改生成的pyd文件名，引入时只引入脚本名部分即可，与直接引入.py脚本时一致。  
**注意：.pyd只能作为外部文件被引入，不能直接运行，需要另写一个.py脚本引入pyd文件作为入口使用。如果.pyd和py同时存在，运行时优先调用.pyd文件。**
![](./python2pydwithmingw\65320813.png)
```python
#引入pyd包
import main_screen_ini_gbk 
#使用pyd中封装好的方法
main_screen_ini_gbk.push2NextHome(tsclibrary) 
```