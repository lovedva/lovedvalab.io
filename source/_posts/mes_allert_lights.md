---
layout: post
title: Esp32开发MES集成警报灯系统,Http远程控制系统设计
tags: [MES,警报灯,远程控制,网页控制,http协议,系统集成,单片机开发,esp32]
categories: [产品方案,系统集成]
date: 2024-3-3 14:10:15
# cover: /assets/xaoxuu/blog/2020-0927a@1x.svg
description:
---

# 核心功能设计
警报灯实机演示:https://www.bilibili.com/video/BV1294y1M7f3?p=2

1. 接受服务器发送http·post请求远程控制警报灯，可接入MES等系统。
2. 提供网页控制端，可手机或电脑等上网设备手动控制警报灯开关。
3. 优化初始配置，可从网页端设置需要连接的Wifi。
4. 警报灯系统带有实体按钮，服务器远程开启警报后，需要人工手动去按按钮关闭警报，以加强警示作用。
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/5_19_37_17_20240305193717.png)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/5_19_38_27_20240305193826.png)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/5_19_35_30_20240305193530.png)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/5_19_34_21_20240305193420.png)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/07/29_20_39_48_7cf396f6f69be4e893de3c7c6c50a44.jpg)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/07/29_20_40_22_4bd8d67d91e01cef99782fa17ed9e8c.jpg)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/07/29_20_40_29_e6d7de9cf36579cf2a740187b2b643f.jpg)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/07/29_20_40_37_73e09b10894ce81378d7577406b797e.jpg)



# 主控设备
单片机ESP32,Micropython或者C语言编程，控制IO引脚从而控制警报灯开关。

# 电路设计
单片机输出针脚——4路继电器——警报灯4个
单片机输出针脚——4个常开按钮
12V电源电源适配器——12V-3.3v电源模块——单片机电源针脚
12V-3.3v电源模块——4路继电器供电

# 程序功能设计

## http服务器后端功能设计
1. 接受http·post请求以控制引脚开关，从而控制警报灯开关。
    ![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/5_19_40_13_20240305194012.png)
2. 设置本系统连接Wifi账号和密码。
    ![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/5_19_50_26_20240305195026.png)
3. 开机默认连接一个固定Wifi名称(default_ssid = "hotspot-2.4G" default_password = "01234567")，用于初始配置，固定Wifi名称可由手机热点生成，方便配置本系统。
    ![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/5_19_48_38_20240305194838.png)
4. Wifi连接超时1分钟后还未连接至指定Wifi，会自动尝试连接以上固定Wifi名称，以重新进行初始配置。

## 网页前端功能设计
1. 显示物理MAC地址
2. 网页端发送http·post请求至后端，以手动控制警报灯开启和关闭。
3. 网页端设置连接Wifi名称和密码，确认后重启系统生效。
   
## 常开按钮程序设计
1. 4个常开按钮，按下后关闭IO引脚以关闭警报灯。

# 外壳设计
电源开孔，常开按钮开孔，固定束带/双面胶
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/5_19_41_10_20240305194110.png)