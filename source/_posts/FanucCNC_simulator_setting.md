---
layout: post
title: 发那科数控机床FanucCNC(NCGuide)仿真模拟器配置,FOCAS采集测试
tags: [虚拟机,数控机床,Fanuc机床,硬件开发,系统集成,工控,数据采集]
categories: [开发记录,硬件开发,系统集成]
date: 2024-3-12 10:10:15
description:
# cover: anime,game
---

## 开发日记5.14
追加NCGuide语言设置为中文

- 3.12
此篇用于记录发那科数控机床(Fanuc CNC)采集程序开发中，用虚拟机做测试时，虚拟机的配置和使用以支持采集软件开发和测试。


## 配置虚拟机使用仿真软件
### 下载VMware15和NCGuide虚拟机文件

「链接：https://pan.xunlei.com/s/VNsl9Gmb14ANBiiNlsT7vA2LA1?pwd=bv2z# 提取码：bv2z”复制这段内容后打开手机迅雷App，查看更方便」  

下载链接失效请私信或者文章最下方评论，QQ465318701 答案:没有 han.dagou@outlook.com  

### 打开包含FANUC NCGuide的虚拟机文件
菜单栏文件——打开——选择对应的虚拟机文件
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_13_45_35_202403121345914.png)

## 配置NCGuide模拟器
### 配置NCGuide面板选项
1. 打开NCGuide文件夹中的 Setting Management，**以Fanuc System Oi-MF型号为例**，选择FSOi-F,
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_14_21_59_202403121421313.png)
2. 右边选项中把机器操作面板中配置中的“主面板+副面板A”打开，这样就有了包含急停的面板。
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_14_30_22_202403121430623.png)

### 配置NCGuide仿真构成选项
1. 进入FS0i-F文件夹，打开 Machine Composition Setting(FS0i-F)，选择3轴机型(M型号)，点击编辑，
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_14_36_51_202403121436453.png)
2. 勾选“启用PMC仿真”，点击OK关闭
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_14_40_48_202403121440914.png)
3. 启动FS0i-F型号的NCGuide，选择刚刚配置过的M型号，点击启动机床模拟器
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_14_46_32_202403121446608.png)

### 配置机床显示语言
1. 按下面板上 [OFS SET] 按钮，选择设置，把写参数项目改为1可写，点击面板中INPUT按钮确认输入值
    ![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/05/14_10_18_55_202405141018322.png)
2. 按下面板上[ SYSTEM ] 按钮，点击PARAMETER参数，输入参数编号3281，按下 SearchNo 搜索编号，然后把数值改成15，之后重启
    ![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/05/14_10_23_34_202405141023864.png)
    ![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/05/14_10_28_45_202405141028933.png)
    ![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/05/14_10_38_37_202405141038984.png)

## 配置xp虚拟机IP地址，使主机可以互通虚拟机IP
1. 关闭xp虚拟机，再VMware菜单选项卡，打开【编辑】——【虚拟网络编辑器】，新建一个NAT模式的虚拟网卡，此处为VMnet8，确保再主机的控制面板——网络适配器设置（网络连接）中可以看到这个同名的网络连接接口。
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_14_53_4_202403121453160.png)
2. 设置虚拟机的网络适配器至刚刚新建的NAT模式的虚拟网卡(此处为VMnet8)，然后再次启动虚拟机。
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_14_59_13_202403121459501.png)
3. 在虚拟机内用控制台查看IP地址，在外部主机可以ping通虚拟机的ip地址即可。
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_15_14_26_202403121514177.png)
![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/12_15_11_13_202403121511419.png)

## 使用采集软件Demo测试与NCGuide的连接
下载地址:https://gitee.com/dvaloveu/Fanuc_CNC_Communicate/releases/tag/v0.1-alpha  
注意添加信任，右边的读取参数功能没开发完，左边的自动刷新的可用，输入虚拟机的IP地址连接成功(端口号与图中默认相同)读取到对应数据即可。
**虚拟机中的NCGuide中不需要设置网卡和IP地址(FOCAS内嵌那些)，全机型都可以模拟通信，不管实际机型是否支持网络通信都可以被FOCAS开发的通信程序采集数据。**

![](https://gitlab.com/lovedva/pic404/-/raw/main/pictures/2024/03/11_22_4_49_20240311220448.png)

