---
layout: post
title: 三菱CNC机床采集程序·FCSB1224驱动开发笔记,C++·MFC开发, 
tags: [三菱CNC,三菱机床,采集程序,驱动开发,C·C++,MFC开发]
categories: [产品方案,系统集成]
date: 2024-3-11 9:08:15
# cover: /assets/xaoxuu/blog/2020-0927a@1x.svg
description:
---

### CLSID类标识符

在COM（Component Object Model）架构中，CLSID（Class Identifier）是一个独特的标识符，用于唯一标识一个COM类。COM是Microsoft的一种技术，用于在不同的软件组件之间实现对象的创建和通信。理解CLSID的用途和重要性，需要了解COM的一些基本概念：

COM对象：

    COM对象是遵循COM规范的封装了数据和功能的实体。每个COM对象都实现了一个或多个接口，而接口是一组功能相关的函数的集合。
    类与类工厂：

    在COM中，每个对象都是由一个特定的类创建的。这个类负责实现对象的实例化逻辑。但是，不是直接创建类的实例，而是通过类工厂（Class Factory）来创建对象。
    CLSID的作用：

    每个COM类都有一个与之相关联的唯一的CLSID。这个CLSID是用来标识特定的类工厂，因此当需要创建一个特定类型的COM对象时，就可以通过其CLSID来找到相应的类工厂。
    在实际使用中，通过CLSID，COM运行时可以查找注册表中的相应信息，以确定如何创建特定的COM对象。
    创建COM对象：

    创建COM对象通常通过调用CoCreateInstance或相关函数实现。这个函数需要CLSID来确定要创建哪个类的实例。
    系统使用CLSID查找注册表中的信息，找到对应的类工厂，然后类工厂创建并返回对象的实例。
    总之，CLSID在COM编程中扮演着关键角色，因为它提供了一种机制，通过这种机制可以找到并创建特定类型的对象实例。通过使用CLSID，COM能够支持不同组件之间的松耦合集成，允许在没有静态链接的情况下动态创建和使用对象。

### CLSIDFromProgID
```
CLSID clsid;
CLSIDFromProgID(L”EZSocketNc.EZNcCommunication”,&clsid);
```
    这段代码是使用COM（Component Object Model）的Windows编程技术中的两行代码。让我们一步一步解释这两行代码：

    CLSID clsid;

    这行代码声明了一个名为 clsid 的 CLSID 类型的变量。CLSID 是一个特殊的数据类型，在COM编程中用于唯一标识一个COM类。它通常是一个结构，包含用于标识COM类的GUID（Globally Unique Identifier）。
    CLSIDFromProgID(L"EZSocketNc.EZNcCommunication", &clsid);

    这行代码调用了 CLSIDFromProgID 函数。CLSIDFromProgID 是一个标准的COM API函数，用于将程序ID（ProgID）转换为其对应的 CLSID。
    L"EZSocketNc.EZNcCommunication" 是ProgID，这是一个人类可读的字符串，用于在系统中标识特定的COM对象。ProgID通常以公司名、项目名等作为前缀，以确保唯一性。
    &clsid 是指向刚刚声明的 CLSID 变量的指针。这个变量在函数调用结束后会被填充对应的 CLSID 值。
    
    这段代码的主要目的是获取与ProgID "EZSocketNc.EZNcCommunication" 关联的 CLSID。在COM中，通常需要知道对象的 CLSID 才能创建该对象的实例。这种将ProgID转换为CLSID的做法使得可以通过人类可读的标识符来寻找和创建COM对象，而不必直接处理更加抽象和不容易记忆的GUID。